package org.example;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.Random.class) // nahodne pořadí
    //@TestMethodOrder(MethodOrderer.Alphanumeric.class) podle abecedy
class FooTest {
    Foo foo;
    @BeforeAll
    public static void beforeAll(){
        System.out.println("Tohle se spustí jen JeDNOU NAZACTKU!!");
    }
    @AfterAll
    public static void afterAll(){
        System.out.println("Tohle se spustí jen JeDNOU NAKONCI!!");
    }
    @BeforeEach
    public void beforeEach(){
         foo = new Foo();
         System.out.println("I run before every test");
    }
    @AfterEach //abychom uvolnili pamet, nebo vratili system do puvodniho stavu
    public void hlaska(){
        System.out.println("I ran as last!");
    }
    @Test
    public void returnsNumber_returns_(){
        //arrange
        //Foo foo = new Foo();

        //ACT-volani metody
        int returned_number=foo.returnNumber();

        //OVERENI (VERIFICATION)
        Assertions.assertEquals(5,returned_number);
    }
    @Test
    public void returnsGet_number(){
        //arrange
        //Foo foo = new Foo();

        //ACT-volani metody
        int get_number=foo.getNum();

        //OVERENI (VERIFICATION)
        Assertions.assertEquals(0,get_number);
    }
    @Test
    public void incrementNum(){
        //arrange
       // Foo foo = new Foo();

        //ACT-volani metody
        foo.increment();

        //OVERENI (VERIFICATION)
        Assertions.assertEquals(1,foo.getNum());
    }

}