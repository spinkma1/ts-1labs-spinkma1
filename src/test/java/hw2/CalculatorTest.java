package hw2;

import org.example.Foo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator cal;
    @BeforeEach
    public void beforeEach(){
        cal = new Calculator();
    }

    @Test
    void add() {
        int result = cal.add(10, 2);
        assertEquals(12, result);
    }

    @Test
    void subtract() {
        int result = cal.subtract(10, 2);
        assertEquals(8, result);
    }

    @Test
    void multiply() {
        int result = cal.multiply(10, 2);
        assertEquals(20, result);
    }

    @Test
    void divide() {
        int result = cal.divide(10, 2);
        assertEquals(5, result);

        try {
            int result2 = cal.divide(10, 0);
        } catch (Exception ex) {
            Assertions.assertNotNull(ex);
        }
    }
}